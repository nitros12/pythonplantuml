from typing import Optional, List, Union
from enum import Enum, auto

from dataclasses import dataclass, field


class ArrowType(Enum):
    extends = ("<|", "|>")
    composition = ("*", "*")
    aggregation = ("o", "o")
    none = ("", "")


class LineType(Enum):
    plain = "--"
    dotted = ".."


@dataclass
class ClassDiagram:
    """A class diagram to group objects and links."""

    objects: list = field(default_factory=list)

    def add_object(self, obj: 'Class'):
        obj.diagram = self
        self.objects.append(obj)

    def add_link(self, link: 'Link'):
        self.objects.append(link)

    def render(self):
        return "\n".join(i.show for i in self.objects)


@dataclass
class Class:
    name: str
    methods: List[str]
    fields: List[str]
    diagram: ClassDiagram = field(init=False, repr=False)


    def add_method(self, meth: str):
        self.methods.append(meth)

    def add_field(self, field_: str):
        self.fields.append(field_)

    def inherits_from(self, other: 'Class') -> 'Link':
        link = Link(other, self, left_ending=ArrowType.extends)
        self.diagram.add_link(link)
        return link

    @property
    def show(self):
        init = f"class {self.name} " + "{"
        meths = "\n".join(f"{{method}} {i}" for i in self.methods)
        fields = "\n".join(f"{{field}} {i}" for i in self.fields)
        end = "}"

        return "\n".join((init, meths, fields, end))


@dataclass
class Object:
    name: str
    fields: dict = field(default_factory=dict)

    @property
    def show(self):
        init = f"object {self.name}" + "{"
        fields = "\n".join(f"{k} = {v}" for k, v in self.fields.items())
        end = "}"

        return "\n".join((init, fields, end))


@dataclass
class Link:
    """A link between two classes."""

    left: str
    right: str
    left_ending: ArrowType = ArrowType.none
    right_ending: ArrowType = ArrowType.none
    line_type: LineType = LineType.plain
    label: Optional[str] = None

    @property
    def show(self):
        ret = f"{self.left} {self.left_ending.value[0]}{self.line_type.value}{self.right_ending.value[1]} {self.right}"
        if self.label is not None:
            ret += f": {self.label}"
        return ret


