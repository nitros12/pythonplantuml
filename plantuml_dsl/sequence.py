from typing import Optional, List, Union
from enum import Enum, auto

from dataclasses import dataclass, field


class ParticipantType(Enum):
    actor = "actor"
    boundary = "boundary"
    control = "control"
    entity = "entity"
    database = "database"


@dataclass
class Participant:
    """A participant in a sequence diagram."""
    name: str
    type: ParticipantType
    alias: Optional[str] = None
    colour: Optional[str] = None
    order: Optional[int] = None

    @property
    def show(self):
        res = f'{self.type} "{self.name}"'
        if self.alias is not None:
            res += f" as {self.alias}"
        if self.colour is not None:
            res += f" #{self.colour}"
        if self.order is not None:
            res += f" order {self.order}"
        return res


@dataclass
class Sequence:
    """Describes a sequence between two objects."""

    left: str
    right: str
    arrow: str = "->"
    message: Optional[str] = None

    @property
    def show(self):
        res = f"{self.left} {self.arrow} {self.right}"
        if self.message is not None:
            res += f" : {self.message}"
        return res


@dataclass
class AutoNumber:
    """Start an autonumbering."""
    start: Optional[int] = None
    step: Optional[int] = None
    format: Optional[str] = None

    @property
    def show(self):
        res = "autonumber"
        if self.start is not None:
            res += f" {self.start}"
        if self.step is not None:
            res += f" {self.step}"
        if self.format is not None:
            res += f" {self.format}"
        return res


@dataclass
class SequenceDiagram:
    """A sequence diagram"""

    entries: List[Union[Participant, Sequence, AutoNumber]]
